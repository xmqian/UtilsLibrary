# 2.0.1
- 修复因为最小版本过低导入项目需要合并版本的问题,最低版本修改为5.0(Api21)
- 修复Andorid10获取IME为null的问题，Manifest增加申请权限==READ_PRIVILEGED_PHONE_STATE==
# 2.0.2
- 新增资源获取类 ResourceUtils
- 新增文字操作类 SpanUtil,标记TextUtils过时,3.0删除
- 新增状态栏操作类StatusBarContralUtils
- 时间工具类TimeUtils进行的整理
- 增加颜色渐变类ColorShades
# 3.0.0
- 2.0.2转为androidx
# 3.0.1
- 删除部分过时的工具类
- 增加文件操作类
- 增加文件夹创建工具类
- 对时间工具类进行优化
- 增加虚拟按键工具类
# 3.0.2
- 优化部分工具类，修复不能进行打包的bug
