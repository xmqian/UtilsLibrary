package com.coszero.uilibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.coszero.uilibrary.R;

/**
 * Created by huihui on 2016/1/26.
 * textView环绕圆形
 */
public class CircleTextView extends AppCompatTextView {
    private final int DEFAULT_BORDER_COLOR = Color.LTGRAY;
    private final int DEFAULT_BORDER_WIDTH = 2;
    private int mBorderColor;
    private int mBorderWidth;
    private Paint paint = new Paint();

    public CircleTextView(Context context) {
        this(context, null);
    }

    public CircleTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleTextView, defStyleAttr, 0);
        mBorderColor = typedArray.getColor(R.styleable.CircleTextView_ctv_border_color, DEFAULT_BORDER_COLOR);
        mBorderWidth = typedArray.getDimensionPixelSize(R.styleable.CircleTextView_ctv_border_width, DEFAULT_BORDER_WIDTH);
        typedArray.recycle();
        init();
    }

    private void init() {
        paint.setColor(mBorderColor);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(mBorderWidth);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int max = Math.max(measuredWidth, measuredHeight);
        setMeasuredDimension(max, max);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (Math.max(getWidth(), getHeight()) / 2) - 2, paint);
    }
}
