package xmqian.myutils.simple

import android.content.pm.PackageManager
import android.os.Environment
import android.view.View
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.coszero.utilslibrary.file.FileDirPathUtils
import com.coszero.utilslibrary.utils.ToastUtils
import xmqian.myutils.R
import xmqian.myutils.common.base.ActivityBase

class FileActivity : ActivityBase() {
    override fun initView() {
        ButterKnife.bind(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_file;
    }

    @OnClick(R.id.createExternalStorageDir)
    fun createExternalStorageDir(v: View) {
        //在外部存储器中创建目录
        val dir = FileDirPathUtils.createExternalStorageDir(this, "createExternalStorageDir")
        ToastUtils.showMsg(dir)
    }

    @OnClick(R.id.createExternalStoragePublicDir)
    fun createExternalStoragePublicDir() {
        //在外部存储器的公共目录中创建目录
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            ToastUtils.showMsg("权限申请成功")
        }
        val dir = FileDirPathUtils.createExternalStoragePublicDir(this, Environment.DIRECTORY_DOWNLOADS, "createExternalStoragePublicDir");
        ToastUtils.showMsg(dir)
    }
}