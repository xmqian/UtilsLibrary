package xmqian.myutils.simple

import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.coszero.uilibrary.dialog.AlertDialog
import com.coszero.uilibrary.dialog.LoadingDialog
import com.coszero.utilslibrary.app.AppUtils
import com.coszero.utilslibrary.utils.ColorShades
import com.coszero.utilslibrary.utils.LogX
import com.coszero.utilslibrary.utils.RandomUtils
import com.coszero.utilslibrary.utils.ToastUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_fragment_toast.*
import xmqian.myutils.Banner
import xmqian.myutils.R
import java.net.URL
import kotlin.concurrent.thread

/**
 * Toast工具类测试
 */
class ToastActivity : AppCompatActivity() {
    private var index = 0
    private val loadingDialog by lazy { LoadingDialog(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_toast)
        ButterKnife.bind(this)
        ctv_get_code?.setOnClickListener { ctv_get_code?.startCountDown(59) }
        tv_http.text = AppUtils.formatColorText(31, 30)
        tv_http.setOnClickListener {
            thread(start = true) {
                val result = URL("http://47.99.140.220/api.php/Xin/Getxin/ads?client=android&package=android.ceshi&version=1.0.0&aid=1&num=1").readText()
                LogX.d("### 请求数据：$result")
                val bannerBean: Banner = Gson().fromJson(result, Banner::class.java)
                LogX.d("#### 解析的数据为：message = ${bannerBean.get()}")
                runOnUiThread {
                    with(bannerBean.data[0]) {
                        tv_http.text = Pic + Name
                    }
                }
            }
            LogX.d("### 请求结束了回调")
        }
        rating_bar.setOnStarTouchListener { touchCount: Int -> textView3.text = "选择了$touchCount 颗星星" }
        textView3.setTextColor(RandomUtils.getRandownColor())

        val colorShades = ColorShades()
        colorShades.forLightShade(Color.BLACK)
        var valueAni: ValueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAni.duration = 5000
        valueAni.interpolator = LinearInterpolator()
        valueAni.addUpdateListener {
            var aniValue = valueAni.animatedValue
            ctv_get_code?.setTextColor(Color.parseColor(colorShades.setShade(aniValue as Float).generateString()))
        }
        valueAni.startDelay = 2000
        valueAni.start()
    }

    @OnClick(R.id.textView2, R.id.textView3)
    fun showShortToast(view: View) {
        index++
        when (view.id) {
            R.id.textView2 -> {
                //        ToastUtils.showMsg(this,"显示短时间显示"+index +"次");
                /*需要注册全局Context*/
                var str = "测试一下换行,符替换问题";
                ToastUtils.showMsg(str.replace(",", "\n") + "显示长时间显示$index 次")
                val alertDialog: AlertDialog = AlertDialog(this)
                alertDialog.builder().setTitle("标题").setMsg("这是一个提示信息")
                        .setPositiveButton("确定") { v -> ToastUtils.showMsg("确定") }
                        .setNegativeButton("quxiao", { ToastUtils.showMsg("取消") }).show()
            }
            R.id.textView3 -> {
                //        ToastUtils.showLongMsg(this,"显示长时间显示"+index +"次");
                /*需要注册全局Context*/
                ToastUtils.showLongMsg("6位随机数：" + RandomUtils.getRandomString(6))
                loadingDialog.content = "加载中"
                when (index) {
                    3 -> loadingDialog.showPD()
                }
            }
        }
    }
}
