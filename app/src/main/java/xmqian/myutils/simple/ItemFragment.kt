package xmqian.myutils.simple

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.coszero.utilslibrary.base.BaseRecyclerAdapter2
import com.coszero.utilslibrary.base.BaseRecyclerViewHolder
import com.coszero.utilslibrary.decoration.HorizontalDividerItemDecoration
import com.coszero.utilslibrary.decoration.SpacingItemDecoration
import com.coszero.utilslibrary.decoration.VerticalDividerItemDecoration
import com.coszero.utilslibrary.device.DeviceNetStateUtils
import com.coszero.utilslibrary.utils.ToastUtils
import xmqian.myutils.simple.dummy.DummyContent
import xmqian.myutils.simple.dummy.DummyContent.DummyItem
import kotlin.properties.Delegates

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ItemFragment.OnListFragmentInteractionListener] interface.
 */
class ItemFragment : Fragment() {
    var aa by Delegates.notNull<String>()
    var kk = mapOf("hei" to 1,
            "bb" to 2,
            "d" to 2)
    // TODO: Customize parameters
    private var columnCount = 2

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onStart() {
        super.onStart()
        DeviceNetStateUtils.registerNetState(activity,true) {
            ToastUtils.showMsg(it)
        }
    }

    override fun onStop() {
        super.onStop()
        DeviceNetStateUtils.unregisterNetState(activity)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(xmqian.myutils.R.layout.fragment_item_list, container, false)
        val rec = view as RecyclerView
        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
//                rec.addItemDecoration(SpacingItemDecoration(columnCount, DensityUtil.dip2px(activity, 10f), false))
                var hline = HorizontalDividerItemDecoration.Builder(activity).size(40).build()
                var vline = VerticalDividerItemDecoration.Builder(activity).size(40).showLastDivider().showLastDivider().build()
                rec.addItemDecoration(hline)
                rec.addItemDecoration(vline)
                rec.addItemDecoration(SpacingItemDecoration(4, 10, false))
                adapter = MyItemRecyclerViewAdapter(DummyContent.ITEMS, listener)
            }
        }
        rec.adapter = object : BaseRecyclerAdapter2<Any>(activity, listOf("我是一号位置", "我是二号位置", "我是三号位置", "四号位置")) {
            override fun getItemLayoutId(viewType: Int): Int {
                return xmqian.myutils.R.layout.fragment_item
            }

            override fun bindData(holder: BaseRecyclerViewHolder?, position: Int, item: Any?) {
                holder?.getTextView(xmqian.myutils.R.id.item_number)?.text = item as String
            }

            override fun getEmptyLayoutId(): Int {
                return xmqian.myutils.R.layout.fragment_item
            }

            override fun onBindViewHolder(holder: BaseRecyclerViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
            }

            override fun setEmptyView(holder: BaseRecyclerViewHolder?) {
                super.setEmptyView(holder)
                holder?.getTextView(xmqian.myutils.R.id.item_number)?.text = "其实我是空的布局，哈哈"
                holder?.getTextView(xmqian.myutils.R.id.item_number)?.setOnClickListener { _ -> ToastUtils.showMsg("围殴之空的") }
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: DummyItem?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
                ItemFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
