package xmqian.myutils.simple

import android.os.Bundle
import com.coszero.utilslibrary.utils.ToastUtils
import xmqian.myutils.R
import xmqian.myutils.common.base.BaseActivity
import xmqian.myutils.simple.dummy.DummyContent

class FragmentActivity : BaseActivity(), ItemFragment.OnListFragmentInteractionListener {
    override fun onListFragmentInteraction(item: DummyContent.DummyItem?) {
        ToastUtils.showLongMsg("测试一波加载")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        initView()
    }

    fun initView() {
        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, ItemFragment()).commit();
    }

}