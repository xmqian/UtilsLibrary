package xmqian.myutils.simple

import android.content.Intent
import com.bumptech.glide.Glide
import com.coszero.utilslibrary.encryption.Base64Utils
import com.yanzhenjie.permission.AndPermission
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_check_input.*
import xmqian.myutils.common.base.ActivityBase
import android.os.Build
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.coszero.utilslibrary.system.ClipboardUtils
import com.coszero.utilslibrary.system.KeyBoardUtils
import com.coszero.utilslibrary.system.SystemOpenAppUtils
import com.coszero.utilslibrary.utils.*


/**
 * @author xmqian
 * @date 2018/10/17 19:20
 * @desc 输入检查
 */
class CheckInputActivity : ActivityBase() {
    override fun initView() {
        btn_input_text.setOnClickListener {
            if (KeyBoardUtils.isKeyBoardOpened(this)) {
                KeyBoardUtils.closeKeybord(edit_input, this)
            } else {
                KeyBoardUtils.openKeybord(edit_input, this)
            }
            ClipboardUtils.copyToClipboard(this, edit_input.text.toString())
        }

        //打开系统浏览器
        btn_open_brower.setOnClickListener {
            SystemOpenAppUtils.openSystemBrowser(this, edit_input.text.toString())
        }
        //打开系统相册
        btn_open_image.setOnClickListener {
            AndPermission.with(this)
                    .runtime()
                    .permission(Permission.CAMERA, Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_EXTERNAL_STORAGE)
                    .onGranted {
                        SystemOpenAppUtils.openSystemImage(this, 2)
                    }
                    .onDenied {
                    }
                    .start()
        }

        btn_clear_clipboard.setOnClickListener { ClipboardUtils.clear(this)}
    }

    override fun onResume() {
        super.onResume()
        this.window.decorView.post {
            var clipboardMessage: CharSequence = ClipboardUtils.getText(this)
            if (!StringUtils.isEmpty(clipboardMessage as String)) {
                edit_input?.setText(clipboardMessage)
            }
        }
    }

    override fun getLayoutId(): Int {
        return xmqian.myutils.R.layout.activity_check_input
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2 && resultCode == RESULT_OK) {
            var photoPath = GetPhotoFromPhotoAlbum.getRealPathFromUri(this, data?.data)
//            var photoPath: Uri? = data?.data
            Glide.with(this).load(photoPath).into(iv_image)
//            val filePath = photoPath?.let { getFilePathFromContentUri(it) }
            LogX.d("### 图片绝对地址为 $photoPath")
            val imageToBase64 = Base64Utils.imageToBase64(photoPath)
            LogX.d("### base64转码后的图片为：$imageToBase64")
            System.out.print(imageToBase64)
        }
    }

    /**
     * Uri转化为文件绝对路径
     *
     * @param selectedVideoUri
     * @param context
     * @return
     */
    private fun getFilePathFromContentUri(selectedVideoUri: Uri): String {
        val filePath: String
        val filePathColumn = arrayOf(MediaStore.MediaColumns.DATA)
        //        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        //      也可用下面的方法拿到cursor
        val cursor = managedQuery(selectedVideoUri, filePathColumn, null, null, null)
        cursor.moveToFirst()
        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
        filePath = cursor.getString(columnIndex)
        return try {
            //4.0以上的版本会自动关闭 (4.0--14;; 4.0.3--15)
            if (Integer.parseInt(Build.VERSION.SDK) < 14) {
                //只有4.0以下才需要手动关闭
                cursor.close()
            }
            filePath
        } catch (e: Exception) {
            Log.d("AppUtils", "####获取文件绝对路径游标错误")
            ""
        }

    }
}
