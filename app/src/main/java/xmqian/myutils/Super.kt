package xmqian.myutils

open class Super()

data class Banner(val result: Int, private val message: String, val data: List<BannerContent>) {
    fun get(): String {
        return message
    }
}

data class BannerContent(val Name: String, val Pic: String, val Url: String, val UrlType: Int, val SystemClass: String, val SystemClassVal: Int)