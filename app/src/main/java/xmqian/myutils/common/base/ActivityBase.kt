package xmqian.myutils.common.base

import android.os.Bundle

/**
 * @author xmqian
 * @date 2018/5/25
 * @desc
 */
abstract class ActivityBase : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initView()
    }

    abstract fun initView()
    protected abstract fun getLayoutId(): Int
}
