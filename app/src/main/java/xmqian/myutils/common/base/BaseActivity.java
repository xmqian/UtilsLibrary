package xmqian.myutils.common.base;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.coszero.utilslibrary.app.AppManager;

/**
 * @author xmqian
 * Created by zhangfeng on 2016/4/28.
 * @deprecated 这个父类代码简单，直接移动到主工程moudle里面就行
 * Activity界面的父类
 */
public abstract class BaseActivity extends AppCompatActivity {
    public static Class<?> currentActivity;
    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
        //setNavigationIcon需要放在 setSupportActionBar之后才会生效。
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentActivity = getClass();
    }

    /**
     * 隐藏状态栏开关
     *
     * @param on
     */
    @TargetApi(19)
    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    /**
     * 退出app
     */
    public void exitApp() {
        AppManager.getAppManager().exitApp(getApplicationContext(), 0);
        System.out.println("#exit app");
    }

    @Override
    protected void onDestroy() {
        AppManager.getAppManager().removeActivity(this);
        super.onDestroy();
    }
}
