package xmqian.myutils.banner

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.coszero.uilibrary.banner.anim.unselect.NoAnimExist
import com.coszero.uilibrary.banner.transform.ZoomOutPageTransformer
import com.coszero.utilslibrary.utils.DensityUtil
import com.coszero.utilslibrary.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_banner.*
import xmqian.myutils.R

class BannerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banner)
        var banners: List<String> = listOf("https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg",
                "https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg",
                "https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg",
                "https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg",
                "https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg")
        vp_branner.viewPager.pageMargin = -DensityUtil.dip2px(this, 75f)
        vp_branner.setSelectAnimClass(NoAnimExist::class.java).setSource(banners)
                .setTransformerClass(ZoomOutPageTransformer::class.java)
                .startScroll()
        vp_branner.setOnItemClickL { position ->
            ToastUtils.showMsg("点击的是第 $position 个")
        }
        var strings: MutableList<String> = arrayOf("第一页",
                "第二页有点长",
                "第三页很长的一段话，测试一下你很长的时候会时怎么样子的",
                "第四页这个吗，看看各种字符和换行吧 \n 这是一个换行不知道使用html会怎样呢").toMutableList()
        looper_textview.setTipList(strings)
        Glide.with(this).load("https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg").into(round_image_view)
    }
}
