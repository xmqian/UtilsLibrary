package xmqian.myutils.banner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.coszero.uilibrary.banner.widget.Banner.BaseIndicatorBanner;
import com.coszero.uilibrary.widget.RoundImageView;

import xmqian.myutils.R;


/**
 * @author xmqian
 * @date 2018/10/19 16:43
 * @desc 轮播图加载控件
 */
public class ComplexImageBannerView extends BaseIndicatorBanner<String, ComplexImageBannerView> {


    public ComplexImageBannerView(Context context) {
        this(context, null, 0);
    }

    public ComplexImageBannerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ComplexImageBannerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onTitleSlect(TextView tv, int position) {
//        final BannersBean item = mDatas.get(position);
        //设置显示文字，没有则不需要显示
//        tv.setText(item.getMs());
    }

    @Override
    public View onCreateItemView(int position) {
        View inflate = View.inflate(mContext, R.layout.item_simple_image, null);
        RoundImageView iv = inflate.findViewById(R.id.image_view);
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        String imgUrl = "https://b-ssl.duitang.com/uploads/item/201711/13/20171113161138_ckam2.jpeg";
        Glide.with(mContext).load(imgUrl).into(iv);
        return inflate;
    }

    float x1, x2, y1, y2;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                x1 = ev.getX();
                y1 = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                x2 = ev.getX();
                y2 = ev.getY();
                if (Math.abs(x2 - x1) < Math.abs(y2 - y1)) {
                    //如果横向距离大于纵向,不拦截触摸事件
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        // getParent().requestDisallowInterceptTouchEvent(true);
        return super.dispatchTouchEvent(ev);
    }
}
