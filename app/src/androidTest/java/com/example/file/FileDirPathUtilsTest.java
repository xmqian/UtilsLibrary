package com.example.file;

import android.content.Context;
import android.os.Environment;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.coszero.utilslibrary.file.FileDirPathUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class FileDirPathUtilsTest {
    private Context appContext;

    @Before
    public void setUp() throws Exception {
        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void createExternalStorageDir() {
        FileDirPathUtils.createExternalStorageDir(appContext, "createExternalStorageDir");
    }

    @Test
    public void createExternalStoragePublicDir() {
        FileDirPathUtils.createExternalStoragePublicDir(appContext, Environment.DIRECTORY_MUSIC, "createExternalStoragePublicDir");
    }

    @Test
    public void createAppExternalFilesDir() {
        FileDirPathUtils.createAppExternalFilesDir(appContext, "createAppExternalFilesDir");
    }

    @Test
    public void createAppExternalCacheDir() {
        FileDirPathUtils.createAppExternalCacheDir(appContext, "createAppExternalCacheDir");
    }

    @Test
    public void createAppCacheDir() {
        FileDirPathUtils.createAppCacheDir(appContext, "createAppCacheDir");
    }

    @Test
    public void createAppFilesDir() {
        FileDirPathUtils.createAppFilesDir(appContext, "createAppFilesDir");
    }
}