package com.example;


import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * @author xmqian
 * @date 19-5-4
 * @desc Android 单元测试类 依赖机器
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestLibrary {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("xmqian.myutils", appContext.getPackageName());
        System.out.println("Android单元测试");
    }
}
