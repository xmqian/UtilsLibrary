package com.example;

import org.junit.Test;

import java.util.regex.Matcher;

/**
 * @author xmqian
 * @date 19-5-4
 * @desc 单元测试 使用JVM
 * Example local unit test, which will execute on the development machine (host).
 */
public class ExampleUnitTestLibrary {

    @Test
    public void test() {
        String bankNo = "6217001630047494627";
        String matcher = Matcher.quoteReplacement("/^(\\d{4})\\d+(\\d{4})$/");
        String rp = bankNo.replace(matcher, "$1 **** **** $2");
    }
}