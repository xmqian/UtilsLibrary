# 开发工具类
- 编写开发时常用的方法,个人用到过的方法会进行封装,且只关联原生安卓库和谷歌官方支持库
- 代码中使用少量字符串资源,字符串资源尽量使用字符串文件调用,方便做多语言
- [类说明详细文档转移至Wiki文档](https://gitee.com/xmqian/UtilsLibrary/wikis/Home)

## app——针对于apk包一些操作或者信息获取
- AppConfig  获取App中的一些主要信息
- [AppCheckUtils——数据检查,防止null](https://gitee.com/xmqian/UtilsLibrary/wikis/AppCheckUtils?sort_id=1578349)
- [AppUtils——apk工具类](https://gitee.com/xmqian/UtilsLibrary/wikis/AppUtils?parent=utils)
- ApkInfoUtils——获取当前apk的信息
- AppManager——App管理类,统一管理Acitivity

## base——常用的原生类二次封装
- BaseTabViewPagerAdapter-FragmentViewPager的封装  

## callback——工具类中需要使用的回调接口
- OnItemClickListener

## encryption——加密解密工具
- Base64Utils——Base64编码,可对图片进行编码
- MD5Utils——MD5加密
- SHAUtils——散列哈希加密

## device——设备信息采集和控制的工具类
- [DeviceNetStateUtils——手机网络状态获取](https://gitee.com/xmqian/UtilsLibrary/wikis/PhoneNetStateUtils?parent=utils)**
- [DeviceScreenInfoUtils——获取手机屏幕信息](https://gitee.com/xmqian/UtilsLibrary/wikis/PhoneScreenInfoUtils?parent=utils)**
- [DeviceInfoGetUtils——手机信息获取](https://gitee.com/xmqian/UtilsLibrary/wikis/PhoneInfoGetUtils?parent=utils)**
- StatusBarContralUtils——状态栏控制
- NavigationBarUtils——虚拟按键工具类

## decoration——RecycleView分割线
- HorizontalDividerItemDecoration——RecycleView分割线横线
- VerticalDividerItemDecoration——RecycleView分割线竖线
- SpacingItemDecoration——RecycleView分割线空白线,可用于List和Gride

## file——文件操作类
- DownloadSaveImg——下载并存储图片
- DownloadSaveImgs——下载多图并存储
- FileUtils——文件操作类
- FileControl——文件控制类,实现删除,创建,复制,移动功能
- FileDirPathUtils——文件夹创建工具类

## system——系统功能的使用
- SystemOpenAppUtils ——调用系统应用
- ClipboardUtils——使用系统剪贴板
- KeyBoardUtils——使用系统软键盘

## utils——比较杂的工具

- [DataCleanManager——数据缓存管理](https://gitee.com/xmqian/UtilsLibrary/wikis/DataCleanUtil?parent=utils%2Fdatacache)
- [DensityUtil——屏幕密度单位转换](https://gitee.com/xmqian/UtilsLibrary/wikis/DensityUtil?parent=utils)
- [LoadingUtils——等待](https://gitee.com/xmqian/UtilsLibrary/wikis/LoadingUtils?parent=utils)
- [LogX——日志打印](https://gitee.com/xmqian/UtilsLibrary/wikis/LogX?parent=utils)
- [EncryptUtils——加密](https://gitee.com/xmqian/UtilsLibrary/wikis/EncryptUtils?parent=utils)
- [PreferenceHelper——存储](https://gitee.com/xmqian/UtilsLibrary/wikis/PreferenceHelper?parent=utils)
- [SharedPreUtils——存储](https://gitee.com/xmqian/UtilsLibrary/wikis/SharedPreUtils?parent=utils)
- [StringUtils——字符串操作类](https://gitee.com/xmqian/UtilsLibrary/wikis/StringUtils?parent=utils)
- [TimeUtils——时间操作类](https://gitee.com/xmqian/UtilsLibrary/wikis/TimeUtils?parent=utils)
- [ToastUtils——Toast提示](https://gitee.com/xmqian/UtilsLibrary/wikis/ToastUtils?parent=utils)
- UpdateManager
- [Utils——杂项工具类](https://gitee.com/xmqian/UtilsLibrary/wikis/Utils?parent=utils)
- [decoration —— RecyclerView分割线](https://gitee.com/xmqian/UtilsLibrary/wikis/decoration?sort_id=1542806)
- ResourceUtils —— 项目内部资源获取
- SpanUtil —— 文字操作类,可进行删除线局部变色等操作
- ColorShades——颜色渐变类
- GetPhotoFromPhotoAlbum——获取系统相机拍照后的文件路径
- RandomUtils——生成随机数据
