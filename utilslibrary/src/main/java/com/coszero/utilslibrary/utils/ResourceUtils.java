package com.coszero.utilslibrary.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.ArrayRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

/**
 * Desc： 资源引入工具类,为了提供版本兼容性
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/5/8 9:46
 */
public class ResourceUtils {
    /**
     * 获取颜色色值资源
     *
     * @param context context
     * @param resourceId 资源id
     * @return 返回颜色色值资源
     */
    public static int getColor(Context context, @ColorRes int resourceId) {
        return ContextCompat.getColor(context, resourceId);
    }

    /**
     * 获取字符串资源
     */
    public static String getString(Context context, @StringRes int resourceId) {
        return context.getResources().getString(resourceId);
    }

    /**
     * 获取字符串数组资源
     *
     * @param resourceId array数组资源id
     */
    public static String[] getStringArray(Context context, @ArrayRes int resourceId) {
        return context.getResources().getStringArray(resourceId);
    }

    /**
     * 获取数字数组资源
     *
     * @param resourceId array数组资源id
     */
    public static int[] getIntArray(Context context, @ArrayRes int resourceId) {
        return context.getResources().getIntArray(resourceId);
    }

    /**
     * 获取字符序列数组资源
     *
     * @param resourceId array数组资源id
     */
    public static CharSequence[] getTextArray(Context context, @ArrayRes int resourceId) {
        return context.getResources().getTextArray(resourceId);
    }

    /**
     * 获取资源
     *
     * @param context context
     * @param resourceId drawableId
     * @return return drawable
     */
    public static Drawable getDrawble(Context context, @DrawableRes int resourceId) {
        return ContextCompat.getDrawable(context, resourceId);
    }

    /**
     * 获取计量数据
     *
     * @param context context
     * @param resourceId dimens资源id
     * @return 返回转换过的数据, 内部有按照屏幕尺寸进行转换
     */
    public static float getDimension(Context context, @DimenRes int resourceId) {
        return context.getResources().getDimension(resourceId);
    }
}
