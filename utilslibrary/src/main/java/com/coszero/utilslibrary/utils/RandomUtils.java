package com.coszero.utilslibrary.utils;

import java.util.Random;

/**
 * Desc： 获取随机数信息
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/7/5 16:07
 */
public class RandomUtils {
    /**
     * @return 返回随机颜色 以 0xff8800格式
     */
    public static int getRandownColor() {
        Random random = new Random();
        int ranColor = 0xff000000 | random.nextInt(0x00ffffff);
        return ranColor;
    }

    /**
     * 获取指定位数的随机字符串(包含小写字母、大写字母、数字,0<length)
     *
     * @param length
     * @return
     */
    public static String getRandomString(int length) {
        //随机字符串的随机字符库
        String KeyString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuffer sb = new StringBuffer();
        int len = KeyString.length();
        for (int i = 0; i < length; i++) {
            sb.append(KeyString.charAt((int) Math.round(Math.random() * (len - 1))));
        }
        return sb.toString();
    }
}
