package com.coszero.utilslibrary.utils;

import android.graphics.Color;

/**
 * Desc： 颜色渐变类
 * <p>
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/4/13 17:42
 * titleColorShades.setFromColor(0xffffffff)
 * .setToColor(0xffff00ff)
 * .setShade(decimal.floatValue());
 *
 * @line(https://gist.github.com/cooltechworks/4f37021b1216f773daf8) Source from :
 * https://gist.github.com/cooltechworks/4f37021b1216f773daf8
 * Color shades will provide all the intermediate colors between two colors. It just requires a decimal value between
 * 0.0 to 1.0
 * and it provides the exact shade combination of the two color with this shade value.
 * Textual explanation :
 * <p>
 * White          LtGray          Gray            DkGray           Black
 * 0               0.25            0.5             0.75            1
 * Given two colors as White and Black,and shade
 * as 0    gives White
 * as 0.25 gives Light gray
 * as 0.5  gives Gray
 * as 0.75 gives Dark gray
 * as 1    gives Black.
 */
public class ColorShades {

    private int mFromColor;
    private int mToColor;
    private float mShade;

    /**
     * 开始颜色色值
     *
     * @param fromColor int色值
     * @return 返回自己
     */
    public ColorShades setFromColor(int fromColor) {
        this.mFromColor = fromColor;
        return this;
    }

    /**
     * 改变的最终色值
     *
     * @param toColor int色值
     * @return 返回自己
     */
    public ColorShades setToColor(int toColor) {
        this.mToColor = toColor;
        return this;
    }

    /**
     * 设置开始色值
     *
     * @param fromColor string 色值 #FFFFFF
     * @return 返回自己
     */
    public ColorShades setFromColor(String fromColor) {

        this.mFromColor = Color.parseColor(fromColor);
        return this;
    }

    /**
     * 设置最终色值
     *
     * @param toColor string色值 #FFFFFF
     * @return 返回自己
     */
    public ColorShades setToColor(String toColor) {
        this.mToColor = Color.parseColor(toColor);
        return this;
    }

    /**
     * 从白色需要渐变的颜色
     *
     * @param color 最终改变的颜色
     * @return 返回自己
     */
    public ColorShades forLightShade(int color) {
        setFromColor(Color.WHITE);
        setToColor(color);
        return this;
    }

    /**
     * 从任意颜色转变到黑色
     *
     * @param color 开始色值
     * @return 返回自己
     */
    public ColorShades forDarkShare(int color) {
        setFromColor(color);
        setToColor(Color.BLACK);
        return this;
    }

    /**
     * 设置渐变比例
     *
     * @param shade 比率 0.1f
     * @return 返回自己
     */
    public ColorShades setShade(float shade) {
        //增加传入异常的数值判断
        if (shade > 1) {
            this.mShade = 1;
            return this;
        }
        if (shade < 0) {
            this.mShade = 0;
            return this;
        }
        this.mShade = shade;
        return this;
    }

    /**
     * 生成渐变色色值
     *
     * @return 返回int值的渐变色
     */
    public int generate() {
        int fromR = Color.red(mFromColor);
        int fromG = Color.green(mFromColor);
        int fromB = Color.blue(mFromColor);

        int toR = Color.red(mToColor);
        int toG = Color.green(mToColor);
        int toB = Color.blue(mToColor);

        int diffR = toR - fromR;
        int diffG = toG - fromG;
        int diffB = toB - fromB;

        int red = fromR + (int) ((diffR * mShade));
        int green = fromG + (int) ((diffG * mShade));
        int blue = fromB + (int) ((diffB * mShade));

        return Color.rgb(red, green, blue);
    }

    /**
     * 反向渐变色，比如设置的是由黑变白，使用此方法是由白变黑，及100%为黑色
     *
     * @return 返回色值
     */
    public int generateInverted() {
        int fromR = Color.red(mFromColor);
        int fromG = Color.green(mFromColor);
        int fromB = Color.blue(mFromColor);

        int toR = Color.red(mToColor);
        int toG = Color.green(mToColor);
        int toB = Color.blue(mToColor);

        int diffR = toR - fromR;
        int diffG = toG - fromG;
        int diffB = toB - fromB;

        int red = toR - (int) ((diffR * mShade));
        int green = toG - (int) ((diffG * mShade));
        int blue = toB - (int) ((diffB * mShade));
        return Color.rgb(red, green, blue);
    }

    /**
     * Gets the String equivalent of the generated shade
     *
     * @return 返回字符串的颜色 #FFFFFF
     */
    public String generateInvertedString() {
        return String.format("#%06X", 0xFFFFFF & generateInverted());
    }

    /**
     * Gets the inverted String equivalent of the generated shade
     *
     * @return 返回反向字符串的颜色 #FFFFFF
     */
    public String generateString() {
        return String.format("#%06X", 0xFFFFFF & generate());
    }
}
