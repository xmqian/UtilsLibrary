package com.coszero.utilslibrary.encryption;

import android.text.TextUtils;
import android.util.Base64;

import com.coszero.utilslibrary.utils.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Desc： Base64编码工具类
 * 1.将图片编码base64,与解码
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/7/31 15:55
 */
public class Base64Utils {
    private Base64Utils() {
        throw new UnsupportedOperationException("u con't instantiate me...");
    }

    /**
     * 去除换行符的base64字符串编码
     *
     * @param path
     * @return
     */
    public static String imageToBase64(String path) {
        return imageToBase64(path, Base64.NO_WRAP);
    }

    /**
     * 将图片转换成Base64编码的字符串
     *
     * @param path
     * @param flage CRLF：这个参数看起来比较眼熟，它就是Win风格的换行符，意思就是使用CR LF这一对作为一行的结尾而不是Unix风格的LF
     * DEFAULT：这个参数是默认，使用默认的方法来加密
     * NO_PADDING：这个参数是略去加密字符串最后的“=”
     * NO_WRAP：这个参数意思是略去所有的换行符（设置后CRLF就没用了）
     * URL_SAFE：这个参数意思是加密时不使用对URL和文件名有特殊意义的字符来作为加密字符，具体就是以-和_取代+和
     * @return base64编码的字符串
     */
    public static String imageToBase64(String path, int flage) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
            File file = new File(path);
            is = new FileInputStream(file);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
                /*CRLF：这个参数看起来比较眼熟，它就是Win风格的换行符，意思就是使用CR LF这一对作为一行的结尾而不是Unix风格的LF
                DEFAULT：这个参数是默认，使用默认的方法来加密
                NO_PADDING：这个参数是略去加密字符串最后的“=”
                NO_WRAP：这个参数意思是略去所有的换行符（设置后CRLF就没用了）
                URL_SAFE：这个参数意思是加密时不使用对URL和文件名有特殊意义的字符来作为加密字符，具体就是以-和_取代+和/*/
            result = Base64.encodeToString(data, flage);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return result;
    }

    /**
     * base64编码字符集转化成图片文件。
     *
     * @param base64Str
     * @param path 文件存储路径
     * @return 是否成功
     */
    public static boolean base64ToFile(String base64Str, String path) {
        byte[] data = Base64.decode(base64Str, Base64.DEFAULT);
        for (int i = 0; i < data.length; i++) {
            if (data[i] < 0) {
                //调整异常数据
                data[i] += 256;
            }
        }
        OutputStream os = null;
        try {
            os = new FileOutputStream(path);
            os.write(data);
            os.flush();
            os.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 默认的base64字符串编码
     *
     * @return
     */
    public static String encodeToStr(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        }
        return Base64.encodeToString(str.getBytes(), Base64.DEFAULT);
    }

    /**
     * 默认的base64字符串解码
     *
     * @return
     */
    public static String decodeToStr(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        }
        byte[] decode = Base64.decode(str.getBytes(), Base64.DEFAULT);
        return new String(decode);
    }
}
