package com.coszero.utilslibrary.system;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import com.coszero.utilslibrary.utils.LogX;

/**
 * Desc： 剪贴板工具类
 * <p>
 *
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/6/2 17:05
 */
public class ClipboardUtils {
    /**
     * 复制数据到剪贴板
     *
     * @param contents 内容
     */
    public static void copyToClipboard(Context context, String contents) {
        ClipboardManager c = (ClipboardManager) context.getSystemService(
                Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("text", contents);
        // 设置Clipboard 的内容
        c.setPrimaryClip(clipData);
    }

    /**
     * 获取剪贴板的文本,使用时需要注意，要在onResum()的生命周期中获取
     * 需要使用下面的代码进行获取,针对Andnrid 10的特殊操作
     * this.getWindow().getDecorView().post(new Runable{});
     *
     * @return 剪贴板的文本
     */
    public static CharSequence getText(Context context) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = cm.getPrimaryClip();
        if (clip != null && clip.getItemCount() > 0) {
            return clip.getItemAt(0).coerceToText(context);
        }
        LogX.d("### ClipboardUtils 剪贴版没有内容");
        return "";
    }

    /**
     * 清空剪切板
     */
    public static void clear(Context context) {
        ClipboardManager manager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (manager != null) {
            try {
                manager.setPrimaryClip(manager.getPrimaryClip());
                manager.setPrimaryClip(ClipData.newPlainText("", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
