package com.coszero.utilslibrary.callback;

import android.view.View;

import com.coszero.utilslibrary.base.BaseRecyclerViewHolder;

/**
 * ClassName: OnItemClickListener<p>
 *
 * @author :oubowu<p>
 * Fuction: 点击长按的接口<p>
 * CreateDate:2016/2/14 1:48<p>
 * UpdateUser:<p>
 * UpdateDate:<p>
 */
public interface OnItemClickListener {
    /**
     * @param view
     * @param position
     * @param holder
     */
    void onItemClick(View view, int position, BaseRecyclerViewHolder holder);
}
