package com.coszero.utilslibrary.device;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;

/**
 * Desc： 状态栏控制,可全屏或者变换状态栏颜色等操作
 * <p>
 *
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/6/2 11:03
 */
public class StatusBarContralUtils {
    /**
     * 改变状态栏的颜色及图标颜色并使用状态栏空间进行布局
     * 设置透明状态栏（API21，5.0之后才能用）
     *
     * @param color 通知栏颜色，完全透明填 Color.TRANSPARENT 即可
     * @param isLightMode 是否为亮色模式（黑色字体，需要6.0 以后支持，否则显示无效）
     */
    public static void changeStatusBarToFullscreen(Activity activity, int color, boolean isLightMode) {
        //大于5.0才设置
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //大于6.0 并且是亮色模式
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isLightMode) {
                activity.getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            } else {
                activity.getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            }
            //因为5.0不支持黑色字体模式，如果设置成透明色则字体回看不到，设置成半透明黑色
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && color == Color.TRANSPARENT) {
                color = Color.parseColor("#44000000");
            }
            activity.getWindow().setStatusBarColor(color);
        }
    }

    /**
     * 隐藏状态栏
     *
     * @param activity activity
     */
    public static void hintStatusBar(Activity activity) {
        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        activity.getWindow().setFlags(flag, flag);
    }

    /**
     * 单修改状态栏颜色
     *
     * @param activity activity
     * @param color color
     */
    public static void changeStatusBarColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(color);
        }
    }
}
