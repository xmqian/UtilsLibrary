package com.coszero.utilslibrary.file;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Desc： 文件控制
 * 通常文件控件的操作有 复制 移动 删除 新增
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/10/12 0012 14:52
 */
public class FileControl {
    // <editor-fold desc="文件复制" defaultstate="collapsed">

    /**
     * 复制文件
     *
     * @param source 输入文件
     * @param target 输出文件
     */
    public static void copy(File source, File target) {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            fileOutputStream = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            while (fileInputStream.read(buffer) > 0) {
                fileOutputStream.write(buffer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 复制文件
     *
     * @param filename 文件名
     * @param bytes 数据
     */
    public static void copy(String filename, byte[] bytes) {
        try {
            //如果手机已插入sd卡,且app具有读写sd卡的权限
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                FileOutputStream output = null;
                output = new FileOutputStream(filename);
                output.write(bytes);
                Log.i("FileUtils", "copy: success" + filename);
                output.close();
            } else {
                Log.i("FileUtils", "copy:fail, " + filename);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 从asset文件夹中复制文件
     *
     * @param assets Context.getAssets()
     * @param source assets文件夹中文件的路径或文件名称
     * @param dest 复制文件的目标路径,绝对路径
     * @throws IOException IOException
     */
    public static void copy(AssetManager assets, String source, String dest)
            throws IOException {
        File file = new File(dest);
        if (!file.exists()) {
            InputStream is = null;
            FileOutputStream fos = null;
            try {
                is = assets.open(source);
                String path = dest;
                fos = new FileOutputStream(path);
                byte[] buffer = new byte[1024];
                int size = 0;
                while ((size = is.read(buffer, 0, 1024)) >= 0) {
                    fos.write(buffer, 0, size);
                }
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } finally {
                        if (is != null) {
                            is.close();
                        }
                    }
                }
            }
        }
    }
    //</editor-fold>

    // <editor-fold desc="文件删除" defaultstate="collapsed">


    /**
     * 删除目录下的所有文件及文件夹
     *
     * @param root 目录
     */
    public static void deleteAllFiles(File root) {
        File files[] = root.listFiles();
        if (files != null)
            for (File f : files) {
                if (f.isDirectory()) { // 判断是否为文件夹
                    deleteAllFiles(f);
                    try {
                        f.delete();
                    } catch (Exception e) {
                    }
                } else {
                    if (f.exists()) { // 判断是否存在
                        deleteAllFiles(f);
                        try {
                            f.delete();
                        } catch (Exception e) {
                        }
                    }
                }
            }
    }
    //</editor-fold>

    // <editor-fold desc="文件新增" defaultstate="collapsed">

    /**
     * 判断是否有该文件夹,没有则创建文件夹
     *
     * @param dirPath 文件路径
     * @return 是否有该文件路径
     */
    public static boolean makeDir(String dirPath) {
        return FileDirPathUtils.makeDir(dirPath);
    }


    //</editor-fold>
}
