package com.coszero.utilslibrary.base.refresh;

import java.util.List;

/**
 * @param <T> 更新数据接口
 * @author xmqian
 * Created by fujinhu on 2017-1-23.
 */
public interface AdapterRefreshInterface<T> {

    /**
     * 刷新数据
     *
     * @param list list
     * @param page page
     */
    void doRefresh(List<T> list, int page);

    /**
     * 结束刷新
     */
    void complete();
}
