package com.coszero.utilslibrary.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.Arrays;
import java.util.List;

/**
 * Desc： FragmentPagerAdapter的简单封装,
 * 可独立使用，也可以继承使用
 * <p>
 *
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/7/11 10:29
 */
public class BaseTabViewPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragments;
    private List<String> mTitles;

    /**
     * @param fm
     * @param fragments
     * @param titles 需要设置 TabLayout时传入的标题
     */
    public BaseTabViewPagerAdapter(FragmentManager fm, @Nullable List<Fragment> fragments, String[] titles) {
        this(fm, fragments, Arrays.asList(titles));
    }

    public BaseTabViewPagerAdapter(FragmentManager fm, @NonNull List<Fragment> fragments, @NonNull List<String> titles) {
        super(fm);
        mFragments = fragments;
        mTitles = titles;
    }

    /**
     * 不需要设置标题时使用
     *
     * @param fm
     * @param fragments
     */
    public BaseTabViewPagerAdapter(FragmentManager fm, @NonNull List<Fragment> fragments) {
        super(fm);
        mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        if (mFragments != null) {
            return mFragments.size();
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mTitles == null)
            return "";
        return mTitles.get(position);
    }

    /**
     * 更新标题时使用，更新时标题数量必须大于界面数量
     *
     * @param position 更新的位置
     * @param title
     */
    public void setPageTitle(int position, String title) {
        if (position >= 0 && position < mTitles.size()) {
            mTitles.set(position, title);
            notifyDataSetChanged();
        }
    }
}
